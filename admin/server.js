// load the things we need
var express = require('express');
var app = express();

app.use(express.static(__dirname + '/public'));
// set the view engine to ejs
app.set('view engine', 'ejs');

// use res.render to load up an ejs view file

// index page
app.get('/', function(req, res) {
    res.render('pages/index', {
        title: 'index'
    });
});
// user_list page
app.get('/user_list', function(req, res) {
    res.render('pages/user_list', {
        title: 'user_list'
    });
});
// commentary page
app.get('/user_commentary', function(req, res) {
    res.render('pages/commentary', {
        title: 'user_commentary'
    });
});

// commentary page
app.get('/user_rating', function(req, res) {
    res.render('pages/rating', {
        title: 'user_rating'
    });
});

// ui ux page
app.get('/ui_ux', function(req, res) {
    res.render('pages/ux', {
        title: 'ui_ux'
    });
});

// commentary page
app.get('/login', function(req, res) {
    res.render('pages/login', {
        title: 'login'
    });
});

app.listen(8080);
console.log('8080 is the magic port');