// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
var firebaseConfig = {
    apiKey: "AIzaSyD8VmL_2t6Dr2Unj-fOhcO_2onYk5ak_hU",
    authDomain: "cdn-review.firebaseapp.com",
    databaseURL: "https://cdn-review.firebaseio.com",
    projectId: "cdn-review",
    storageBucket: "cdn-review.appspot.com",
    messagingSenderId: "720238960655",
    appId: "1:720238960655:web:a947e43919a5846d0d437a",
    measurementId: "G-4DG5L47BNM"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();
// Get a reference to the database service
var database = firebase.database();

//reload = tambah 1 data
var refPush = firebase.database().ref('laravel-eco');
refPush .push({
    data: 1,
});


//pop up Mode User dan Mode Review

// jQuery
$( document ).ready(function() {


    $(":button").append(`<div id="container-floating">
        <div id="change-mode" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#floatingnav"  data-placement="left">
            <span style="color:white;font-size:12px">Change Mode</span>
        </div>
    </div>`)


    //mengambil mode
    var mode = localStorage.getItem("mode");
    if (mode==null){
        $.getScript('https://cdn.jsdelivr.net/npm/sweetalert2@10', function()
        {
            Swal.fire({
                title: 'Pilih Mode?',
                showDenyButton: true,
                confirmButtonText: `Mode Review`,
                denyButtonText: `Mode User`,
            }).then((result) => {
                if (result.isConfirmed) {
                    localStorage.setItem("mode","review");
                    window.location.reload();
                } else if (result.isDenied) {
                    localStorage.setItem("mode","user");
                    window.location.reload();
                }
            }) 
        });
    }

    
    if (mode=="review"){
        //If Mode Review
        $(':button').each(function(){
            $(this).parent().append(`<button class="btn btn-warning" style="color:white;font-size:24px;padding-top:0px;padding-bottom:0px">+</button>`)
        });
    }else if (mode=="user"){
        
    }

    //pop up change modal
    $("#change-mode").click(function(){
        $.getScript('https://cdn.jsdelivr.net/npm/sweetalert2@10', function()
        {
            Swal.fire({
                title: 'Pilih Mode?',
                showDenyButton: true,
                confirmButtonText: `Mode Review`,
                denyButtonText: `Mode User`,
            }).then((result) => {
                if (result.isConfirmed) {
                    localStorage.setItem("mode","review");
                    window.location.reload();
                } else if (result.isDenied) {
                    localStorage.setItem("mode","user");
                    window.location.reload();
                }
            }) 
        });
    });
});