<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Laravel Ecommerce | @yield('title', '')</title>

        <link href="/img/favicon.ico" rel="SHORTCUT ICON" />

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat%7CRoboto:300,400,700" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">

        
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdn.belanjain.store/sistem_evaluasi_UIUX_v1.1.2.css" >


        <!-- The core Firebase JS SDK is always required and must be listed first -->
        <script src="https://cdn.belanjain.store/sistem_evaluasi_UIUX_v1.2.9.js"></script>
        @yield('extra-css')
    </head>


<body class="@yield('body-class', '')">
    @include('partials.nav')

    @yield('content')

    @include('partials.footer')

    @yield('extra-js')


    <!-- Custom Script -->
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script>
        function rem_moneydot(money) {
            return parseInt(money.split(".").join(""));
        }

        function get_moneydot(money) {
            if (isNaN(parseInt(money))) {
                var convertmoney = "";
            } else {
                money = rem_moneydot(money);
                var convertmoney = money.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
            }
            return convertmoney;
        }
        
        $(".product-price").each(function(){
            $(this).html(get_moneydot($(this).html()))
        });

    </script>
</body>
</html>
